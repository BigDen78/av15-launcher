package org.radioavionica.dmitryz.av15launcher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import java.io.File;

/**
 * Created by dmitryz on 5/30/16.
 */
public class UpdateListener implements View.OnClickListener {
    private File file;
    private Context context;
    public UpdateListener(File f,Context c){
        file = f;
        context = c;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}

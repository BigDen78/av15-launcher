package org.radioavionica.dmitryz.av15launcher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Button searchButton = (Button) findViewById(R.id.updateButton);
        searchButton.setOnClickListener(mSearchListener);

        Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private View.OnClickListener mSearchListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            List<File> files = getFilesOfDirectory(new File("/sdcard/"),".av15");

            LinearLayout myLayout = (LinearLayout) findViewById(R.id.updatesLayout);
            myLayout.removeAllViews();
            for (File f:files) {
                View view = getLayoutInflater().inflate(R.layout.table_element, null);
                final TextView text = (TextView) view.findViewById(R.id.updateName);
                text.setText(f.getName());
                Button installButton = (Button) view.findViewById(R.id.installButton);

                installButton.setOnClickListener(new UpdateListener(f,getBaseContext()));
                myLayout.addView(view);
            }
        }
    };

    public static List<File> getFilesOfDirectory(File dir, String fileNamePart) {

        List<File> result;

        if (dir.exists() && dir.isDirectory()
                && !dir.getAbsolutePath().contains("root")
                && dir.getAbsolutePath().split("/").length < 10) {
            //Log.i("fileSearch", "search in directory " + dir.getAbsolutePath());
            File[] files = dir.listFiles();

            if (files != null)
                result = new ArrayList<File>(Arrays.asList(files));
            else
                result = new ArrayList<File>();
        }

        else
            result = Collections.emptyList();

        List<File> filteredResult = new ArrayList<File>();
        for (File file : result) {
            if (file.getAbsolutePath().toLowerCase()
                    .contains(fileNamePart.toLowerCase()))
                filteredResult.add(file);
        }
        return filteredResult;
    }

}

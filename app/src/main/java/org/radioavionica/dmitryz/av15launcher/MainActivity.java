package org.radioavionica.dmitryz.av15launcher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import device.service.*;

public class MainActivity  extends AppCompatActivity {
    DeviceService service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        service = new DeviceService(getApplicationContext());
        try {
            service.setNavigationBarHide(true);
            service.setOnlyHardKeyboardEnabled(true);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        Button launchButton = (Button)findViewById(R.id.launchButton);
        Button updateButton = (Button)findViewById(R.id.updateButton);
        Button settingsButton = (Button)findViewById(R.id.settingsButton);

        launchButton.setOnClickListener(mLaunchListener);
        updateButton.setOnClickListener(mUpdateListener);
        settingsButton.setOnClickListener(mSettingsListener);

    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private View.OnClickListener mSettingsListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
        }
    };

    private View.OnClickListener mLaunchListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Log.d("App","Launch");
            if(isPackageInstalled("com.radioavionica.avicon15",getApplicationContext())) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.radioavionica.avicon15");
                startActivity(launchIntent);
            }
        }
    };

    private View.OnClickListener mUpdateListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Log.d("App","Update");
            Intent myIntent = new Intent(MainActivity.this, UpdateActivity.class);
            startActivity(myIntent);

        }
    };

}
